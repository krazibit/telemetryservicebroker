﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using TelemetryServiceBroker.ClientCommunication.Models;
using TelemetryServiceBroker.Config;
using TelemetryServiceBroker.DAL;

namespace TelemetryServiceBroker.Utility
{
    public enum TokenValidationResult
    {
        Valid,
        Invalid,
        UnexpectedIp,
        TokenNotFound,
        Expired
    }

    public class AuthenticationManager
    {
      
        private const char SinatraChar = '_';
        public static CurrentIssuedToken GetToken(AuthenticationRequest request)
        {
            Dictionary<string, string> authorizedApps = new Dictionary<string, string>();
            var config = TelemetryServiceBrokerSection.Instance;
            if (config.AuthorizedApplications != null && config.AuthorizedApplications.Count > 0)
                authorizedApps = config.AuthorizedApplications.Cast<AuthorizedApplication>()
                    .Select(x => new {x.AppName, x.SecretKey})
                    .ToDictionary(x => x.AppName, x => x.SecretKey);
            if (request == null ||
                !(authorizedApps.ContainsKey(request.HostAppName) &&
                  authorizedApps[request.HostAppName].Equals(request.ApiKey)))
                throw new UnauthorizedAccessException();

            var currentIssuedToken = TsbDbManager.Instance.GetCurrentIssuedToken(request.Username, request.ExpectedIp,
                request.HostAppName);
            if (currentIssuedToken != null && currentIssuedToken.ExpirationDate > DateTime.Now)
                return currentIssuedToken;

            var tokenValidityPeriodSeconds = TelemetryServiceBrokerSection.Instance.JwtTokenValidityPeriod;
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var expirationTime = DateTime.Now.AddSeconds(tokenValidityPeriodSeconds); 
            var expirationEpoch = Math.Round((expirationTime.ToUniversalTime() - unixEpoch).TotalSeconds);
            var secretKey = TelemetryServiceBrokerSection.Instance.JwtSecret;
            var payload = GenerateTokenPayLoad(request,expirationEpoch);
            var token = JWT.JsonWebToken.Encode(payload, secretKey, JWT.JwtHashAlgorithm.HS256);

            currentIssuedToken = new CurrentIssuedToken
            {
                ExpirationDate = expirationTime,
                HostAppName = request.HostAppName,
                IssueDate = DateTime.Now,
                Token = token,
                UserIpAddress = request.ExpectedIp,
                Username = request.Username
            };

            TsbDbManager.Instance.SaveCurrentIssuedToken(currentIssuedToken);

            return currentIssuedToken;
        }
        
        public static TokenValidationResult ValidateToken(string token, string requestIpAddress)
        {
            try
            {
                var secretKey = TelemetryServiceBrokerSection.Instance.JwtSecret;
                var payLoadObject = JWT.JsonWebToken.DecodeToObject(token, secretKey) as IDictionary<string, object>;
                if(payLoadObject==null)
                    return TokenValidationResult.Invalid;

                var username = payLoadObject["username"] as string;
                var sinatra = payLoadObject["sinatra"] as string;
                var expectedIp = ConvertFromSinatra(sinatra);
                var hostAppName = payLoadObject["appName"] as string;

                if (!expectedIp.Equals(requestIpAddress))
                    return TokenValidationResult.UnexpectedIp;

                var currentIssuedToken = TsbDbManager.Instance.GetCurrentIssuedToken(username, expectedIp, hostAppName);

                if(currentIssuedToken==null)
                    return TokenValidationResult.TokenNotFound;

                if(currentIssuedToken.ExpirationDate <= DateTime.Now)
                    return TokenValidationResult.Expired;

                return TokenValidationResult.Valid;

            }
            catch (System.Exception)
            {
                return TokenValidationResult.Invalid;
            }
        }


        private static object GenerateTokenPayLoad(AuthenticationRequest request, double expirationEpoch)
        {
            var sinatra = ConvertToSinatra(request.ExpectedIp);
            return new Dictionary<string, object>
            {
                {"exp", expirationEpoch},
                {"username", request.Username},
                {"appName", request.HostAppName},
                {"sinatra", sinatra}
            };
        }

        private static string ConvertToSinatra(string ipString)
        {
            var sinatraPlain = ipString.Replace('.', SinatraChar);
            return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(sinatraPlain));
        }

        private static string ConvertFromSinatra(string sinatra)
        {
            var ipSinatra = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(sinatra));
            return ipSinatra.Replace(SinatraChar, '.');
        }
    }
}

﻿using System;

namespace TelemetryServiceBroker.Events
{
    public class SubscriptionEventArgs : EventArgs
    {
        public SubscriptionEventArgs(string accountId, string platform)
        {
            AccountId = accountId;
            Platform = platform;
        }
        public String AccountId { get; set; }
        public String Platform { get; set; }
    }

    public class NewTelemetryDataEventArgs : EventArgs
    {
        public string AccountId { get; set; }
        public string Platform { get; set; }
        public string Data { get; set; }
    }

}
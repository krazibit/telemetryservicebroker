﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using TelemetryServiceBroker.ClientCommunication.Hub;
using TelemetryServiceBroker.Events;
using TelemetryServiceBroker.ServiceChannel;

namespace TelemetryServiceBroker
{
    public class TelemetryServiceBroker
    {
        private readonly ITelemetryServiceChannel _serviceChannel;
        private static readonly ILog Logger = LogManager.GetLogger(typeof (TelemetryServiceBroker));
        //TODO:Use Atomic Increment/Decrement Class with inner long using Interlocked.Incr/Decr
        private readonly Dictionary<string, long> _subscriptionToCountDictionary = new Dictionary<string, long>();
        private readonly TelemetryServiceHubManager _serviceHubManager = TelemetryServiceHubManager.Instance;
        private readonly object _subscriptionDictionaryLock = new object();
        private bool _isStopping;
        public const string SubscriptionKeySeparator = "<=>";


        public TelemetryServiceBroker(ITelemetryServiceChannel serviceChannel)
        {
            _isStopping = false;
            _serviceChannel = serviceChannel;
            _serviceChannel.Connected += ServiceChannelOnConnected;
            _serviceChannel.Disconnected += ServiceChannelOnDisconnected;
            _serviceChannel.NewDataReceived += ServiceChannelOnNewDataReceived;
            _serviceHubManager.ClientSubscribed += ServiceHubManagerOnClientSubscribed;
            _serviceHubManager.ClientUnsubscribed += ServiceHubManagerOnClientUnsubscribed;
        }
        
        public bool ServiceChannelConnected { get; set; }

        private void ServiceHubManagerOnClientUnsubscribed(object sender, SubscriptionEventArgs subscriptionEventArgs)
        {
            if (subscriptionEventArgs != null)
            {
                Logger.DebugFormat("Unsubscribtion request for {0} through channel {1}", subscriptionEventArgs,
                    _serviceChannel.Name);
                var subscriptionKey = subscriptionEventArgs.AccountId + subscriptionEventArgs.Platform;
                if (_subscriptionToCountDictionary.ContainsKey(subscriptionKey))
                {
                    lock (_subscriptionDictionaryLock)
                    {
                        _subscriptionToCountDictionary[subscriptionKey]--;
                    }
                    if (_subscriptionToCountDictionary[subscriptionKey] == 0)
                        _serviceChannel.Unsubscribe(subscriptionEventArgs.AccountId, subscriptionEventArgs.Platform);
                }
            }
        }

        private void ServiceHubManagerOnClientSubscribed(object sender, SubscriptionEventArgs subscriptionEventArgs)
        {
            if (subscriptionEventArgs != null)
            {
                Logger.DebugFormat("Recieved Subscription request for {0} through channel {1}", subscriptionEventArgs, _serviceChannel.Name);
                var subscriptionKey = subscriptionEventArgs.AccountId + subscriptionEventArgs.Platform;

                if (_subscriptionToCountDictionary.ContainsKey(subscriptionKey))
                {
                    lock (_subscriptionDictionaryLock)
                        _subscriptionToCountDictionary[subscriptionKey]++;
                }
                else
                    _subscriptionToCountDictionary[subscriptionKey] = 1;

                if (_subscriptionToCountDictionary[subscriptionKey] == 1)
                    _serviceChannel.Subscribe(subscriptionEventArgs.AccountId, subscriptionEventArgs.Platform);
            }
        }

        private void ServiceChannelOnNewDataReceived(object sender, NewTelemetryDataEventArgs newTelemetryDataEventArgs)
        {
            if(newTelemetryDataEventArgs!=null)
                _serviceHubManager.OnData(newTelemetryDataEventArgs.AccountId, newTelemetryDataEventArgs.Platform,  newTelemetryDataEventArgs.Data);
        }

        private void ServiceChannelOnDisconnected(object sender, EventArgs eventArgs)
        {
            ServiceChannelConnected = false;
            Logger.DebugFormat("Service channel {0} disconnected", _serviceChannel.Name);
            if(!_isStopping)
                _serviceChannel.StartReconnect();
        }

        private void ServiceChannelOnConnected(object sender, EventArgs eventArgs)
        {
            Logger.DebugFormat("Service channel {0} Connected", _serviceChannel.Name);
            ServiceChannelConnected = true;
        }

        public void Start()
        {
            Logger.DebugFormat("Starting Telemetry Service broker with Channel {0}", _serviceChannel.Name);
            _isStopping = false;
            if(_serviceChannel!=null)
                _serviceChannel.Connect();
        }

        public void Stop()
        {
            Logger.DebugFormat("Stopping Telemetry Service broker with Channel {0}", _serviceChannel.Name);
            _isStopping = true;
            if(_serviceChannel!=null)
                _serviceChannel.Disconnect();
        }

    }
}

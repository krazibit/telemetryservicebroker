﻿using System;
using System.Net;
using System.Web.Http;
using TelemetryServiceBroker.ClientCommunication.Models;
using AuthenticationManager = TelemetryServiceBroker.Utility.AuthenticationManager;

namespace TelemetryServiceBroker.ClientCommunication.Controllers
{
    public class AuthenticationController : ApiController
    {
        public IHttpActionResult Post([FromBody] AuthenticationRequest request)
        {
            try
            {
                var token = AuthenticationManager.GetToken(request);
                if (token == null)
                    return Content(HttpStatusCode.BadRequest, new HttpError("System Error"));
                return
                    Ok(new AuthenticationResult(token.Token,
                        token.ExpirationDate.ToUniversalTime().ToString("yyyy-MM-dd hh:mm:ss.fff"), "", true));
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
            catch (System.Exception ex)
            {
                //TODO: Log
                Console.WriteLine(ex);
                return Content(HttpStatusCode.BadRequest, new HttpError("Operation Error"));
            }
        }
    }
}

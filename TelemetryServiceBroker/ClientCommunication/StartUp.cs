﻿using System.Web.Http;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Owin;
using TelemetryServiceBroker.ClientCommunication.Hub;

[assembly:log4net.Config.XmlConfigurator(Watch = true)]
namespace TelemetryServiceBroker.ClientCommunication
{
    public class StartUp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            //appBuilder.UseWelcomePage();
            ConfigureWebApi(ref appBuilder);
        }

        private void ConfigureWebApi(ref IAppBuilder appBuilder)
        {
            var httpConfiguration = new HttpConfiguration();
            httpConfiguration.Routes.MapHttpRoute(
                name: "TelemetryServiceBrokerApi",
                routeTemplate: "tsb/{controller}/{id}",
                defaults: new {id = RouteParameter.Optional});

            appBuilder.UseWebApi(httpConfiguration);
            appBuilder.UseCors(CorsOptions.AllowAll);
            GlobalHost.HubPipeline.AddModule(new HubLoggingPipelineModule()); 
            appBuilder.MapSignalR();
        }
    }
}

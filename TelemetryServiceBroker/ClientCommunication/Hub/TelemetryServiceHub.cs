﻿using System.Threading.Tasks;
using log4net;
using Microsoft.AspNet.SignalR;


namespace TelemetryServiceBroker.ClientCommunication.Hub
{
    
    public class TelemetryServiceHub : Microsoft.AspNet.SignalR.Hub
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (TelemetryServiceHub));
        private readonly TelemetryServiceHubManager _telemetryServiceHubManager = TelemetryServiceHubManager.Instance;
    
        public void Subscribe(string token, string accountId, string platform)
        {
            var connId = Context.ConnectionId;
            var ipAddr = GetIpAddress();
            Logger.DebugFormat(
                "Received subscription request from {0} on host [{1}] for Account [{2}] on Platform [{3}]", connId,
                ipAddr, accountId, platform);
            _telemetryServiceHubManager.Subscribe(connId, token, accountId, platform, ipAddr);
        }

        public override Task OnConnected()
        {
            Logger.DebugFormat("Client {0} Connected",Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Logger.DebugFormat("Client {0} Disconnected", Context.ConnectionId);
            _telemetryServiceHubManager.Unsubscribe(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }
        
        public override Task OnReconnected()
        {
            Logger.DebugFormat("Client {0} Reconnected", Context.ConnectionId);
            return base.OnReconnected();
        }

        protected string GetIpAddress()
        {
            string ipAddress;
            object tempObject;

            Context.Request.Environment.TryGetValue("server.RemoteIpAddress", out tempObject);

            if (tempObject != null)
            {
                ipAddress = (string)tempObject;
            }
            else
            {
                ipAddress = "";
            }

            return ipAddress;
        }
    }
}

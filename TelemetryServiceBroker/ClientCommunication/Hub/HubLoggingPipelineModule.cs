﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNet.SignalR.Hubs;

namespace TelemetryServiceBroker.ClientCommunication.Hub
{
    public class HubLoggingPipelineModule : HubPipelineModule
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (HubLoggingPipelineModule));
        
        protected override void OnIncomingError(ExceptionContext exceptionContext, IHubIncomingInvokerContext invokerContext)
        {
            Logger.Error("Hub Error", exceptionContext.Error);
            base.OnIncomingError(exceptionContext, invokerContext);
        }
    }
}

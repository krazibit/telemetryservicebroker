﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using TelemetryServiceBroker.Events;
using TelemetryServiceBroker.Utility;

namespace TelemetryServiceBroker.ClientCommunication.Hub
{
   public class TelemetryServiceHubManager
    {
        private static TelemetryServiceHubManager _instance;
        private readonly Dictionary<string, List<string>> _subscriberToTopicsDictionary = new Dictionary<string, List<string>>();
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TelemetryServiceHubManager));
        private const char Separator = '`';
        private readonly IHubContext _hubContext = GlobalHost.ConnectionManager.GetHubContext<TelemetryServiceHub>();

        public event EventHandler<SubscriptionEventArgs> ClientSubscribed; 
        public event EventHandler<SubscriptionEventArgs> ClientUnsubscribed;
        public event EventHandler<NewTelemetryDataEventArgs> NewDataArrived;
 
        public static TelemetryServiceHubManager Instance {get
        {
            return _instance ?? (_instance = new TelemetryServiceHubManager());
        }}

        public void Subscribe(string connectionId, string token, string accountId, string platform, string clientIp)
        {
            //TODO: Check Failed Attempts and Block Momentarily to avoid DDOS
            ValidateTokenAsync(token, clientIp)
                .ContinueWith(task =>
                {
                    var message = new { accountId = accountId, platform = platform };
                    switch (task.Result)
                    {
                        case TokenValidationResult.Valid:
                            AddToSubscribers(connectionId, accountId, platform);
                            break;
                        case TokenValidationResult.Expired:
                            NotifyOnError(connectionId, "TOKEN_EXPIRED", JsonConvert.SerializeObject(message));
                            break;
                        case TokenValidationResult.TokenNotFound:
                        case TokenValidationResult.Invalid:
                        case TokenValidationResult.UnexpectedIp:
                            Logger.ErrorFormat("Error [{0}] [{1}], Possible Forgery", Enum.GetName(task.Result.GetType(), task.Result), clientIp);
                            NotifyOnError(connectionId, "TOKEN_VALIDATION_ERROR", JsonConvert.SerializeObject(message));
                            break;
                        default:
                            NotifyOnError(connectionId, "TOKEN_VALIDATION_ERROR", JsonConvert.SerializeObject(message));
                            break;
                    }
                });
        }

        public void Unsubscribe(string connectionId)
        {
            if (_subscriberToTopicsDictionary.ContainsKey(connectionId))
            {
                foreach (
                    var acctIdPlatformArr in
                        _subscriberToTopicsDictionary[connectionId].Select(topic => topic.Split(Separator)))
                {
                    Logger.DebugFormat("Unsubscribing {0} from telemetry for accountId {1} on platform {2}", connectionId, acctIdPlatformArr[0], acctIdPlatformArr[1]);
                    if (ClientUnsubscribed != null)
                        ClientUnsubscribed(this, new SubscriptionEventArgs(acctIdPlatformArr[0], acctIdPlatformArr[1]));
                }
            }
        }

        public void OnData(string accountId, string platform, string message)
        {
            Logger.DebugFormat("New Telemetry Data, Account: [{0}]; Platform[{1}] Message: [{2}]", accountId, platform, message);
           
            var topic = GetTopic(accountId, platform);
            var subscribers = _subscriberToTopicsDictionary.Where(x => x.Value.Contains(topic)).Select(x => x.Key);
            Parallel.ForEach(subscribers, s =>
            {
                Logger.DebugFormat("Notifying Subscriber {0}", s);
                _hubContext.Clients.Client(s).onData(accountId, platform, message);
            });
            if (NewDataArrived != null)
                NewDataArrived(this, new NewTelemetryDataEventArgs {AccountId = accountId, Platform  = platform, Data = message});
        }

       private string GetTopic(string accountId, string platform)
       {
           return accountId + Separator + platform;
       }

        private void NotifyOnError(string connectionId, string errorType, string errorMessage)
        {
            Logger.DebugFormat("Notifying client[{0}] on Error, Type[{1}]; Message[{2}]", connectionId, errorType, errorMessage);
           _hubContext.Clients.Client(connectionId).onError(errorType, errorMessage);
        }

        private void AddToSubscribers(string connectionId, string accountId, string platform)
        {
            Logger.DebugFormat("Subscribing {0} to telemetry for accountId {1} on platform {2}", connectionId, accountId, platform);
            
            var topic = GetTopic(accountId, platform);

            if (_subscriberToTopicsDictionary.ContainsKey(connectionId))
            {
                if (!_subscriberToTopicsDictionary[connectionId].Contains(topic))
                    _subscriberToTopicsDictionary[connectionId].Add(topic);
            }
            else
                _subscriberToTopicsDictionary[connectionId] = new List<string> { topic };
            if (ClientSubscribed != null)
                ClientSubscribed(this, new SubscriptionEventArgs(accountId, platform));
        }

        private async Task<TokenValidationResult> ValidateTokenAsync(string token, string clientIp)
        {
            var validationResult = await Task.Run(() => AuthenticationManager.ValidateToken(token, clientIp));
            return validationResult;
        }
        
    }
}

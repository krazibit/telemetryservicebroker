﻿using Newtonsoft.Json;

namespace TelemetryServiceBroker.ClientCommunication.Models
{
    public class Error
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}

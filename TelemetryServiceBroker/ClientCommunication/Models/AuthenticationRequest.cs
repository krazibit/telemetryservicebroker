﻿using Newtonsoft.Json;

namespace TelemetryServiceBroker.ClientCommunication.Models
{
    public class AuthenticationRequest
    {
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("hostAppName")]
        public string HostAppName { get; set; }
        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }
        [JsonProperty("expectedIp")]
        public string ExpectedIp { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace TelemetryServiceBroker.ClientCommunication.Models
{
    public class AuthenticationResult
    {
        public AuthenticationResult(string token, string expiretimeUTC, string message, bool success)
        {
            Token = token;
            Success = success;
            ExpiretimeUTC = expiretimeUTC;
            Message = message;
        }
        
        public AuthenticationResult() { }
        
        [JsonProperty("token")]
        public string Token { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("expiretimeUTC")]
        public string ExpiretimeUTC { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}

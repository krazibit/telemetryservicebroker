﻿using System.Data.Entity;

namespace TelemetryServiceBroker.DAL
{
    public class TsbDbContext : DbContext
    {
        public TsbDbContext() : base("TsbDbContext") { }

        public DbSet<CurrentIssuedToken> CurrentIssuedTokens { get; set; }
 
    }
}

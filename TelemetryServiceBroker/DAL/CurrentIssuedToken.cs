﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TelemetryServiceBroker.DAL
{
    [Table("CurrentIssuedToken")]
    public class CurrentIssuedToken
    {
        [Key, Column(Order = 0)]
        public string Username { get; set; }
        [Key, Column(Order = 1)]
        public string HostAppName { get; set; }
        [Key, Column(Order = 2)]
        public string UserIpAddress { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Token { get; set; }
    }
}

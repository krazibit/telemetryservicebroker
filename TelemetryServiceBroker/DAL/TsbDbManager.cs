﻿using System.Data.Entity;
using System.Linq;

namespace TelemetryServiceBroker.DAL
{
    public class TsbDbManager
    {
        private static TsbDbManager _instance;
        public static TsbDbManager Instance { get { return (_instance ?? (_instance = new TsbDbManager())); } }

        public CurrentIssuedToken GetCurrentIssuedToken(string username, string ipAddress, string hostAppName)
        {
            using (var dbContext =  new TsbDbContext())
            {
                var currentIssueToken =
                    dbContext.CurrentIssuedTokens.FirstOrDefault(
                        x =>
                            x.Username.Equals(username) && x.UserIpAddress.Equals(ipAddress) &&
                            x.HostAppName.Equals(hostAppName));
                return currentIssueToken;
            }
        }

        public bool SaveCurrentIssuedToken(CurrentIssuedToken newIssuedToken)
        {
            using (var dbContext = new TsbDbContext())
            {
                var existingRecord = dbContext.CurrentIssuedTokens.FirstOrDefault(x =>
                    x.Username.Equals(newIssuedToken.Username) && x.UserIpAddress.Equals(newIssuedToken.UserIpAddress) &&
                    x.HostAppName.Equals(newIssuedToken.HostAppName));
                if (existingRecord!=null)
                {
                    dbContext.CurrentIssuedTokens.Remove(existingRecord);
                    dbContext.Entry(existingRecord).State = EntityState.Deleted;
                    dbContext.SaveChanges();
                }
                
                dbContext.CurrentIssuedTokens.Add(newIssuedToken);
                dbContext.Entry(newIssuedToken).State = EntityState.Added;
                return dbContext.SaveChanges() > 0;
            }
        }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelemetryServiceBroker.ServiceChannel
{
    public class ServiceChannelSettings
    {
        public string ServerEndPoint { get; set; }
        public int ServerPort { get; set; }
        public int ReconnectInterval { get; set; }
        public bool LogConnection { get; set; }
        public bool IsEncrypted { get; set; }
    }
}

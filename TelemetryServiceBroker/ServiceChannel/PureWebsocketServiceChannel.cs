﻿using System;
using System.Threading.Tasks;
using Alchemy;
using Alchemy.Classes;
using log4net;
using Newtonsoft.Json;
using TelemetryServiceBroker.Events;

namespace TelemetryServiceBroker.ServiceChannel
{
    public class PureWebsocketServiceChannel:ITelemetryServiceChannel
    {
        private WebSocketClient _wsClient;
        private readonly string _serverAddress;
        private static readonly ILog Logger = LogManager.GetLogger(typeof (PureWebsocketServiceChannel));
        private const string ServiceName = "PureWebsocketClientService";
        private const string SubscriptionRequestType = "SUBSCRIBE";
        private const string UnsubscriptionRequestType = "UNSUBSCRIBE";
        private bool _isReconnecting;
        private readonly int _reconnectInterval = 1000;

        public PureWebsocketServiceChannel(ServiceChannelSettings settings)
        {
            _isReconnecting = false;
            _reconnectInterval = settings.ReconnectInterval > _reconnectInterval ? settings.ReconnectInterval : _reconnectInterval;
            _serverAddress = String.Format("{0}://{1}:{2}/", settings.IsEncrypted ? "wss" : "ws",
                settings.ServerEndPoint, settings.ServerPort);
        }
        
        public void Subscribe(string accountId, string platform)
        {
            var subscriptionKey = platform + TelemetryServiceBroker.SubscriptionKeySeparator + accountId;
            dynamic parameter = new {Pattern = subscriptionKey};
            var subscriptionMessage = new TelemetryServiceRequest(SubscriptionRequestType,
                JsonConvert.SerializeObject(parameter));
            SendMessage(JsonConvert.SerializeObject(subscriptionMessage));
        }
        
        public void Unsubscribe(string accountId, string platform)
        {
            var subscriptionKey = platform + TelemetryServiceBroker.SubscriptionKeySeparator + accountId;
            dynamic parameter = new { Pattern = subscriptionKey };
            var unsubscriptionMessage = new TelemetryServiceRequest(UnsubscriptionRequestType,
                JsonConvert.SerializeObject(parameter));
            SendMessage(JsonConvert.SerializeObject(unsubscriptionMessage));
        }

        public void Connect()
        {
            _wsClient = new WebSocketClient(_serverAddress);
            _wsClient.OnConnected += OnConnected;
            _wsClient.OnDisconnect += OnDisconnect;
            _wsClient.OnReceive += OnReceive;
            _wsClient.OnSend += OnSend;
            _wsClient.OnFailedConnection += OnFailedConnection;
            _wsClient.Connect();
        }

        private void OnFailedConnection(UserContext context)
        {
            if (!_isReconnecting)
                StartReconnect();
        }

        public void Disconnect()
        {
            _isReconnecting = false;
           if(_wsClient!= null && _wsClient.ReadyState == WebSocketClient.ReadyStates.OPEN)
               _wsClient.Disconnect();
        }

        public void StartReconnect()
        {
            _isReconnecting = true;
            Task.Factory.StartNew(() =>
            {
                while (_wsClient!=null && _wsClient.ReadyState != WebSocketClient.ReadyStates.OPEN && _isReconnecting)
                {
                    Connect();
                    Task.Delay(_reconnectInterval);
                }
            });
        }

        public string Name
        {
            get { return ServiceName; }
        }

        public event EventHandler<Events.NewTelemetryDataEventArgs> NewDataReceived;

        public event EventHandler Connected;

        public event EventHandler Disconnected;

        #region Helpers
        private void SendMessage(string message)
        {
            if (_wsClient.ReadyState == WebSocketClient.ReadyStates.OPEN)
            {
                _wsClient.Send(message);
            }
            else
            {
                Logger.ErrorFormat("Trying to send message [{0}], while client is not connected", message);
            }
        } 
        #endregion
        
        #region EventHandlers

        private void OnSend(UserContext context)
        {
        }
        private void OnReceive(UserContext context)
        {
            var rawMessage = context.DataFrame.ToString();
            dynamic message = new { Topic = "", Message = "" };
            var serializedMsg = JsonConvert.DeserializeAnonymousType(rawMessage, message);
            var accountIdPlatformSplit =
                serializedMsg.Topic.ToString()
                    .Split(new[] {TelemetryServiceBroker.SubscriptionKeySeparator}, StringSplitOptions.None);
            var platform = accountIdPlatformSplit[0];
            var accountId = accountIdPlatformSplit[2];
            if (NewDataReceived != null)
                NewDataReceived(this,
                    new NewTelemetryDataEventArgs()
                    {
                        Data = serializedMsg.Message,
                        Platform = platform,
                        AccountId = accountId
                    });
        }

        private void OnDisconnect(UserContext context)
        {
            if (Disconnected != null)
                Disconnected(this, null);
        }

        private void OnConnected(UserContext context)
        {
            _isReconnecting = false;
            if (Connected != null)
                Connected(this, null);
        } 

        #endregion
    }
}

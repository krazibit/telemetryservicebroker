﻿using System;
using System.Threading.Tasks;
using log4net;
using StackExchange.Redis;
using TelemetryServiceBroker.Events;

namespace TelemetryServiceBroker.ServiceChannel
{
    public class RedisServiceChannel : ITelemetryServiceChannel
    {
        private ConnectionMultiplexer _redisClient;
        private readonly string _serverAddress;
        private const string ServiceName = "RedisClient";
        private static readonly ILog Logger = LogManager.GetLogger(typeof (RedisServiceChannel));

        public RedisServiceChannel(ServiceChannelSettings settings)
        {
            _serverAddress = String.Format("{0}:{1}",settings.ServerEndPoint,settings.ServerPort);
        }

        private void RedisClientOnErrorMessage(object sender, RedisErrorEventArgs redisErrorEventArgs)
        {
            Logger.ErrorFormat("Error Message From Redis Client on {0}\n{1}",redisErrorEventArgs.EndPoint, redisErrorEventArgs.Message);
        }

        private void RedisClientOnConnectionRestored(object sender, ConnectionFailedEventArgs connectionFailedEventArgs)
        {
            Logger.ErrorFormat("Redis Service Client Connection Restored ");
            if (Connected != null)
                Connected(this, null);
        }

        private void RedisClientOnConnectionFailed(object sender, ConnectionFailedEventArgs connectionFailedEventArgs)
        {
            Logger.ErrorFormat(
                "Redis Client Connection Failed. ConnectionType[{0}], EndPoint[{1}], FailureType[{3}]\n{2}",
                connectionFailedEventArgs.ConnectionType, connectionFailedEventArgs.EndPoint,
                connectionFailedEventArgs.Exception, connectionFailedEventArgs.FailureType);
            if (Disconnected != null)
                Disconnected(this, null);
        }

        public void Subscribe(string accountId, string platform)
        {
            if(_redisClient==null)
                return;
            string subscriptionKey = platform + TelemetryServiceBroker.SubscriptionKeySeparator + accountId; 
            ISubscriber subscriber = _redisClient.GetSubscriber();
            subscriber.Subscribe(subscriptionKey, OnDataHandler);
        }

        private void OnDataHandler(RedisChannel redisChannel, RedisValue redisValue)
        {
            if (NewDataReceived != null)
            {
                var accountPlatformSplit =
                    redisChannel.ToString()
                        .Split(new [] {TelemetryServiceBroker.SubscriptionKeySeparator}, StringSplitOptions.None);
                var platform = accountPlatformSplit[0];
                var accountId = accountPlatformSplit[1];
                NewDataReceived(this, new NewTelemetryDataEventArgs {Platform = platform, AccountId = accountId, Data = redisValue});
            }
        }

        public void Unsubscribe(string accountId, string platform)
        {
            if(_redisClient==null)
                return;
            var subscriptionKey = platform + TelemetryServiceBroker.SubscriptionKeySeparator + accountId;
            ISubscriber subscriber = _redisClient.GetSubscriber();
            subscriber.Unsubscribe(subscriptionKey);
        }

        public void Connect()
        {
            string connectionString = String.Format("{0},abortConnect=false", _serverAddress);
            _redisClient = ConnectionMultiplexer.Connect(connectionString);
            _redisClient.ConnectionFailed += RedisClientOnConnectionFailed;
            _redisClient.ConnectionRestored += RedisClientOnConnectionRestored;
            _redisClient.ErrorMessage += RedisClientOnErrorMessage;
            PollConnectionStatus();
        }

        private void PollConnectionStatus()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    if (_redisClient != null && _redisClient.IsConnected)
                    {
                        if (Connected != null)
                            Connected(this, null);
                        break;
                    }
                    Task.Delay(1000);
                }
            });
        }

        public void Disconnect()
        {
            _redisClient.Dispose();
        }

        public void StartReconnect()
        {
        }

        public string Name
        {
            get { return ServiceName; }
        }

        public event EventHandler<Events.NewTelemetryDataEventArgs> NewDataReceived;

        public event EventHandler Connected;

        public event EventHandler Disconnected;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TelemetryServiceBroker.ServiceChannel
{
   public class TelemetryServiceRequest
    {
       [JsonProperty(PropertyName = "RequestType")]
       public string RequestType { get; set; }
       [JsonProperty(PropertyName = "Parameters")]
       public string Parameters { get; set; }

       public TelemetryServiceRequest()
       {
           
       }

       public TelemetryServiceRequest(string requestType, string parameters)
       {
           RequestType = requestType;
           Parameters = parameters;
       }
    }
}

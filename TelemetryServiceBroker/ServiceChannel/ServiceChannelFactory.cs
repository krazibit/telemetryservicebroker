﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace TelemetryServiceBroker.ServiceChannel
{
    public class ServiceChannelFactory
    {
        private const string RedisChannel = "REDIS";
        private const string PureWebSocketChannel = "WEBSOCKET";
        private const string SignalrChannel = "SIGNALR";

        public static ITelemetryServiceChannel GetServiceChannel(string type, ServiceChannelSettings settings)
        {
            switch (type.ToUpper())
            {
                case RedisChannel : 
                    return new RedisServiceChannel(settings);
                case PureWebSocketChannel :
                    return new PureWebsocketServiceChannel(settings);
                case SignalrChannel :
                    return new SignalrServiceChannel(settings);
                default :
                    return null;
            }
        }
    }
}

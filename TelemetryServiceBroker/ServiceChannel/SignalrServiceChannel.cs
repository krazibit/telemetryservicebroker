﻿using System;
using System.IO;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;
using TelemetryServiceBroker.Events;

namespace TelemetryServiceBroker.ServiceChannel
{
    public class SignalrServiceChannel:ITelemetryServiceChannel
    {
        private IHubProxy _hubProxy;
        private HubConnection _hubConnection;
        private string _serverAddress;
        private bool _logHubConnection;
        private StreamWriter _hubConnectionWriter;
        private const string ServiceName = "SignalrServiceChannel";
        private const string HubName = "TelemetryServiceHub";
        private const string SubscriptionRequestType = "SUBSCRIBE";
        private const string UnsubscriptionRequestType = "UNSUBSCRIBE";
        private static readonly ILog Logger = LogManager.GetLogger(typeof (SignalrServiceChannel));

        public SignalrServiceChannel(ServiceChannelSettings settings)
        {
            _logHubConnection = settings.LogConnection;
            _serverAddress = string.Format("{0}://{1}:{2}/signalr", settings.IsEncrypted ? "https" : "http",
                settings.ServerEndPoint, settings.ServerPort);
        }
        
        public void Subscribe(string accountId, string platform)
        {
            var subscriptionKey = platform + TelemetryServiceBroker.SubscriptionKeySeparator + accountId;
            dynamic parameter = new { Pattern = subscriptionKey };
            var subscriptionMessage = new TelemetryServiceRequest(SubscriptionRequestType,
                JsonConvert.SerializeObject(parameter));
            SendRequest(subscriptionMessage.RequestType, subscriptionMessage.Parameters);
        }

        public void Unsubscribe(string accountId, string platform)
        {
            var subscriptionKey = platform + TelemetryServiceBroker.SubscriptionKeySeparator + accountId;
            dynamic parameter = new { Pattern = subscriptionKey };
            var unsubscriptionMessage = new TelemetryServiceRequest(UnsubscriptionRequestType,
                JsonConvert.SerializeObject(parameter));
            SendRequest(unsubscriptionMessage.RequestType, unsubscriptionMessage.Parameters);
        }

        public void Connect()
        {
            SetupAndStartHub();
        }

        public void Disconnect()
        {
            if(_hubConnection.State == ConnectionState.Connected)
                _hubConnection.Dispose();
            if (Disconnected != null)
                Disconnected(this, null);
        }

        public void StartReconnect()
        {
            //TODO: Handling reconnection, Auto/Manual? , KeepAlive needed ?
            Logger.Info("Starting to reconnect");
        }

        public string Name
        {
            get { return ServiceName; }
        }


        #region Helpers

        private async void SetupAndStartHub()
        {
            _hubConnection = new HubConnection(_serverAddress);
            _hubConnection.Closed += HubConnectionOnClosed;
            _hubConnection.ConnectionSlow += HubConnectionOnConnectionSlow;
            _hubConnection.Error += HubConnectionOnError;
            _hubConnection.Reconnected += HubConnectionOnReconnected;
            _hubConnection.Reconnecting += HubConnectionOnReconnecting;
            _hubConnection.StateChanged += HubConnectionOnStateChanged;
            _hubConnection.TraceLevel = TraceLevels.All;
            _hubConnection.TraceWriter = Console.Out;
            _hubProxy = _hubConnection.CreateHubProxy(HubName);
            _hubProxy.On<string, string>("broadcast", OnData);
            if (_logHubConnection)
            {
                _hubConnectionWriter = new StreamWriter(String.Format("HubConnection_{0}.log", DateTime.Now.ToString("yyyyMMdd_hh_mm_ss")));
                _hubConnection.TraceLevel = TraceLevels.All;
                _hubConnection.TraceWriter = _hubConnectionWriter;
                _hubConnectionWriter.AutoFlush = true;
            }

            await _hubConnection.Start();
        }

        private void OnData(string topic, string message)
        {
            if (NewDataReceived != null)
            {
                var topicSplit = topic.Split(new string[]{TelemetryServiceBroker.SubscriptionKeySeparator},StringSplitOptions.None);
                var accountId = topicSplit[0];
                var platform = topicSplit[1];
                NewDataReceived(this, new NewTelemetryDataEventArgs { AccountId = accountId, Platform = platform, Data = message});
            }
        }

        private void SendRequest(string requestType, string parameters)
        {
            if (_hubProxy != null)
                _hubProxy.Invoke("Request", requestType, parameters);
            else
                Logger.ErrorFormat("Trying to send rquest, type[{0}]; Param [{1}] while hubproxy is null", requestType,
                    parameters);
        } 
        #endregion

        #region Hub Event handlers
        private void HubConnectionOnStateChanged(StateChange stateChange)
        {
            Logger.DebugFormat("SignalChannel State Changed From {0} to {1}",
                Enum.GetName(stateChange.OldState.GetType(), stateChange.OldState),
                Enum.GetName(stateChange.NewState.GetType(), stateChange.NewState));
            switch (stateChange.NewState)
            {
                case ConnectionState.Connected:
                    if (Connected != null)
                        Connected(this, null);
                    break;
                case ConnectionState.Disconnected:
                    if (Disconnected != null)
                        Disconnected(this, null);
                    break;
            }
        }

        private void HubConnectionOnReconnecting()
        {
            Logger.Debug("Signalr Service Channel reconnecting... ");
        }

        private void HubConnectionOnReconnected()
        {
            Logger.Debug("Signalr Service Channel reconnected");
        }

        private void HubConnectionOnError(System.Exception exception)
        {
            Logger.Error("Signalr Service Channel Error", exception);
        }

        private void HubConnectionOnConnectionSlow()
        {
            Logger.Debug("Signalr Service Channel Connection is Slow");
        }

        private void HubConnectionOnClosed()
        {
            Logger.Debug("Signalr Service Channel Connection is Closed");
        }
        #endregion

        #region Events
        public event EventHandler<NewTelemetryDataEventArgs> NewDataReceived;

        public event EventHandler Connected;

        public event EventHandler Disconnected; 
        #endregion
    }
}

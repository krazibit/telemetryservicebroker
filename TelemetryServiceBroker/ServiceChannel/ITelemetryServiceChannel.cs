﻿using System;
using TelemetryServiceBroker.Events;

namespace TelemetryServiceBroker.ServiceChannel
{
    public interface ITelemetryServiceChannel
    {
        void Subscribe(string accountId, string platform);
        void Unsubscribe(string accountId, string platform);
        void Connect();
        void Disconnect();
        void StartReconnect();
        string Name { get; }

        event EventHandler<NewTelemetryDataEventArgs> NewDataReceived;
        event EventHandler Connected;
        event EventHandler Disconnected;
    }
}

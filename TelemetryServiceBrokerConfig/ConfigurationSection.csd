﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="d0ed9acb-0435-4532-afdd-b5115bc4d562" namespace="TelemetryServiceBroker.Config" xmlSchemaNamespace="TelemetryServiceBroker.Config" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="TelemetryServiceBrokerSection" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="telemetryServiceBroker">
      <attributeProperties>
        <attributeProperty name="ListeningPort" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="listeningPort" isReadOnly="false" documentation="Port to connect to this application">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="JwtSecret" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="jwtSecret" isReadOnly="false" documentation="Secret for signing JWTs">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="JwtIssuerSignature" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="jwtIssuerSignature" isReadOnly="false" documentation="Signature to use as issuer in JWTs">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="JwtAlgorithm" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="jwtAlgorithm" isReadOnly="false" documentation="Algorithm to sign JWTs. [ HS256,HS384,HS512], Deafult HS256" defaultValue="&quot;HS256&quot;">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="JwtTokenValidityPeriod" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="jwtTokenValidityPeriod" isReadOnly="false" documentation="Validity Period of issued JWT in seconds, Default 1800" defaultValue="1800">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="ListeningAddress" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="listeningAddress" isReadOnly="false" documentation="ServiceBroker EndPoint Address">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="UseEncryptedConnection" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="useEncryptedConnection" isReadOnly="false" documentation="Encrypt Connection between clients and broker" defaultValue="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Boolean" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <elementProperties>
        <elementProperty name="AuthorizedApplications" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="authorizedApplications" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AuthorizedApplications" />
          </type>
        </elementProperty>
        <elementProperty name="ServiceChannelSetting" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="serviceChannelSetting" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/ServiceChannelSetting" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElement name="ServiceChannelSetting">
      <attributeProperties>
        <attributeProperty name="Type" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="type" isReadOnly="false" documentation="Receiving Channel Type {Redis,Websocket,Signalr}">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="EndPointAddress" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="endPointAddress" isReadOnly="false" documentation="Endpoint Address of Telemetry Service">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="EndPointPort" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="endPointPort" isReadOnly="false" documentation="Endpoint port of Telemetry Service">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="ReconnectionInterval" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="reconnectionInterval" isReadOnly="false" documentation="Reconnection Interval" defaultValue="0">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="UseEncryptedConnection" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="useEncryptedConnection" isReadOnly="false" documentation="Encrypt connection between broker and provider" defaultValue="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Boolean" />
          </type>
        </attributeProperty>
        <attributeProperty name="LogConnection" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="logConnection" isReadOnly="false" documentation="Works with signalr only" defaultValue="true">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Boolean" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElementCollection name="AuthorizedApplications" xmlItemName="authorizedApplication" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AuthorizedApplication" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="AuthorizedApplication">
      <attributeProperties>
        <attributeProperty name="AppName" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="appName" isReadOnly="false" documentation="Authorized Application Name">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="SecretKey" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="secretKey" isReadOnly="false" documentation="Authorized Application Secret Key">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>
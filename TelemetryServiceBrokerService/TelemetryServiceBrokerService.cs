﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Microsoft.Owin.Hosting;
using Microsoft.Owin.Host.HttpListener;
using TelemetryServiceBroker.ClientCommunication;
using TelemetryServiceBroker.ServiceChannel;

[assembly: log4net.Config.XmlConfigurator(Watch=true)]
namespace TelemetryServiceBrokerService
{
    public partial class TelemetryServiceBrokerService : ServiceBase
    {
        private TelemetryServiceBroker.ServiceChannel.ITelemetryServiceChannel _serviceChannel;
        private TelemetryServiceBroker.TelemetryServiceBroker _serviceBroker;
        private IDisposable _server;
        private ILog Logger = LogManager.GetLogger(typeof (TelemetryServiceBrokerService));

        public TelemetryServiceBrokerService()
        {
            InitializeComponent();
            InitializeService();
        }

        private void InitializeService()
        {
            try
            {
                var config = TelemetryServiceBroker.Config.TelemetryServiceBrokerSection.Instance;
                var channelSettings = new ServiceChannelSettings
                {
                    LogConnection = config.ServiceChannelSetting.LogConnection,
                    ReconnectInterval = config.ServiceChannelSetting.ReconnectionInterval,
                    ServerEndPoint = config.ServiceChannelSetting.EndPointAddress,
                    IsEncrypted = config.ServiceChannelSetting.UseEncryptedConnection,
                    ServerPort = config.ServiceChannelSetting.EndPointPort
                };
                _serviceChannel = ServiceChannelFactory.GetServiceChannel(config.ServiceChannelSetting.Type,
                    channelSettings);
                _serviceBroker = new TelemetryServiceBroker.TelemetryServiceBroker(_serviceChannel);
            }
            catch (Exception ex)
            {
                Logger.Error("Exception Initializing Service", ex);
                throw;
            }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Logger.Info("Starting up Telemetry Service Broker");
                var config = TelemetryServiceBroker.Config.TelemetryServiceBrokerSection.Instance;
                string uri = String.Format("{0}://{1}:{2}", config.UseEncryptedConnection ? "https" : "http",
                    config.ListeningAddress, config.ListeningPort);
                Logger.InfoFormat("Starting up Telemetry Service Broker Listening Address on [{0}]", uri);
                _server = WebApp.Start<StartUp>(uri);
                _serviceBroker.Start();
            }
            catch (Exception ex)
            {
                Logger.Error("Exception Starting Up Telemetry Service Broker", ex);
                Stop();
            }
        }

        protected override void OnStop()
        {
            Logger.Info("Stopping Telemetry Service Broker");
            if(_serviceBroker!=null)
                _serviceBroker.Stop();
            if(_server!=null)
                _server.Dispose();
        }
    }
}
